var gulp = require('gulp');

gulp.task('default', ['scripts', 'styles'], function() {
});

// JS concat, strip debugging and minify
gulp.task('scripts', function() {
  var concat = require('gulp-concat');
  var stripDebug = require('gulp-strip-debug');
  var uglify = require('gulp-uglify');
  var notify = require("gulp-notify");

  var files = [
    'scripts/src/jquery-2.1.1.js',
    'scripts/src/highcharts.js',
    'scripts/src/handlebars.runtime-v2.0.0.js',
    'scripts/src/jquery.modal.min.js',
    'templates/dist/templates.js',
    'scripts/src/global.js'
  ];
  gulp.src(files)
    .pipe(concat('scripts.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('scripts/dist/'))
    .pipe(notify({
      title: 'GULP',
      message: 'Tarefa de scripts finalizada!'
    }));
});

// CSS concat, auto-prefix and minify
gulp.task('styles', function() {
  var concat = require('gulp-concat');
  var autoprefix = require('gulp-autoprefixer');
  var minifyCSS = require('gulp-minify-css');
  var notify = require("gulp-notify");

  var files = [
    'styles/src/fonts.css',
    'styles/src/global.css',
    'styles/src/smartphone.css'
  ];

  gulp.src(files)
    .pipe(concat('styles.css'))
    .pipe(autoprefix('last 2 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('styles/dist/'))
    .pipe(notify({
      title: 'GULP',
      message: 'Tarefa de styles finalizada!'
    }));
});

gulp.task('templates', function(){
  var handlebars = require('gulp-handlebars');
  var wrap = require('gulp-wrap');
  var declare = require('gulp-declare');
  var concat = require('gulp-concat');

  gulp.src('templates/src/*.html')
    .pipe(handlebars())
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'templates',
      noRedeclare: true, // Avoid duplicate declarations
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('templates/dist/'));
});

gulp.task('watch', function() {
  var watch = require('gulp-watch');

  gulp.watch('scripts/src/*.js', ['scripts']);
  gulp.watch('styles/src/*.css', ['styles']);
  gulp.watch('templates/src/*.html', ['templates']);
});
