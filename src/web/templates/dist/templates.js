this["templates"] = this["templates"] || {};
this["templates"]["modalMessage"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div id=\"modal-message\" class=\"modal-container modal-message\">\n	<p>"
    + escapeExpression(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"message","hash":{},"data":data}) : helper)))
    + "</p>\n</div>\n";
},"useData":true});
this["templates"]["modalPanic"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"modal-panic\" class=\"modal-container modal-confirmation\">\n	<div class=\"modal-header\">\n		<h2 class=\"modal-title\">Panic!</h2>\n	</div>\n	<div class=\"modal-content\">\n		<p>Are you sure? This action will completely disable my network control functions and will affect all users in the network.</p>\n		<div class=\"modal-action clearfix\">\n			<a href=\"#\" class=\"btn btn-no\">No</a>\n			<a href=\"#\" class=\"btn btn-yes\">Yes</a>\n		</div>\n	</div>\n</div>\n";
  },"useData":true});
this["templates"]["modalService"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"modal-service\" class=\"modal-container modal-confirmation\">\n	<div class=\"modal-header\">\n		<h2 class=\"modal-title\">Servidor</h2>\n	</div>\n	<div class=\"modal-content\">\n		<p>O servidor está desligado! Você gostaria de religar?</p>\n		<div class=\"modal-action no-option clearfix\">\n			<a href=\"#\" class=\"btn btn-yes\">Yes</a>\n		</div>\n	</div>\n</div>\n";
  },"useData":true});
this["templates"]["userTable"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", buffer = "<tr class=\"data "
    + escapeExpression(((helpers.check_id || (depth0 && depth0.check_id) || helperMissing).call(depth0, (depth0 != null ? depth0.user_id : depth0), {"name":"check_id","hash":{},"data":data})))
    + " "
    + escapeExpression(((helpers.check_priority || (depth0 && depth0.check_priority) || helperMissing).call(depth0, (depth0 != null ? depth0.has_priority : depth0), {"name":"check_priority","hash":{},"data":data})))
    + "\">\n	<td>\n		<span class=\"status-icon\"></span>\n	</td>\n	<td>"
    + escapeExpression(((helper = (helper = helpers.rank || (depth0 != null ? depth0.rank : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"rank","hash":{},"data":data}) : helper)))
    + "</td>\n	<td>"
    + escapeExpression(((helper = (helper = helpers.user_id || (depth0 != null ? depth0.user_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"user_id","hash":{},"data":data}) : helper)))
    + "</td>\n	<td>"
    + escapeExpression(((helper = (helper = helpers.instant_rate || (depth0 != null ? depth0.instant_rate : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"instant_rate","hash":{},"data":data}) : helper)))
    + " "
    + escapeExpression(((helper = (helper = helpers.user_rate_unit || (depth0 != null ? depth0.user_rate_unit : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"user_rate_unit","hash":{},"data":data}) : helper)))
    + "</td>\n	<td>"
    + escapeExpression(((helper = (helper = helpers.instant_percentage || (depth0 != null ? depth0.instant_percentage : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"instant_percentage","hash":{},"data":data}) : helper)))
    + " %</td>\n</tr>\n<tr class=\"timer "
    + escapeExpression(((helpers.check_id || (depth0 && depth0.check_id) || helperMissing).call(depth0, (depth0 != null ? depth0.user_id : depth0), {"name":"check_id","hash":{},"data":data})))
    + " "
    + escapeExpression(((helpers.check_priority || (depth0 && depth0.check_priority) || helperMissing).call(depth0, (depth0 != null ? depth0.has_priority : depth0), {"name":"check_priority","hash":{},"data":data})))
    + "\">\n	<td colspan=\"5\" class=\"time\">\n		<div class=\"time-wrapper\">\n			<div class=\"priority-time\" style=\"width:"
    + escapeExpression(((helper = (helper = helpers.priority_remaining_fraction || (depth0 != null ? depth0.priority_remaining_fraction : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"priority_remaining_fraction","hash":{},"data":data}) : helper)))
    + "%\"></div>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.priority_remaining_time : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</div>\n	</td>\n</tr>\n";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			<div class=\"tooltip\">"
    + escapeExpression(((helper = (helper = helpers.priority_remaining_time || (depth0 != null ? depth0.priority_remaining_time : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"priority_remaining_time","hash":{},"data":data}) : helper)))
    + "</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, depth0, {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});