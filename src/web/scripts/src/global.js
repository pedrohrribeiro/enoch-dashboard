var site = (function($) {

  var self = this;

  var exports = {};

  options = {};

  var fn = {};

  var $userTableDownload = $('.user-table-download tbody'),
      $userTableUpload = $('.user-table-upload tbody'),
      $totalInstantDownloadRate = $('.total-instant-download-rate'),
      $totalInstantDownloadFill = $('.total-instant-download-fill'),
      $totalInstantDownloadPointer = $('.total-instant-download-pointer'),
      $totalInstantUploadRate = $('.total-instant-upload-rate'),
      $totalInstantUploadFill = $('.total-instant-upload-fill'),
      $totalInstantUploadPointer = $('.total-instant-upload-pointer'),
      $activeUsersCount = $('.active-users-count'),
      $totalPriorities = $('.total-priorities'),
      $totalDislikes = $('.total-dislikes'),
      $totalDownload = $('.total-download'),
      $totalUpload = $('.total-upload'),
      $since = $('.since'),
      $board = $('.board'),
      $graph = $('.graph-container'),
      $btnDislike = $('.btn-dislike'),
      $btnPanic = $('.btn-panic'),
      $btnPriority = $('.btn-priority'),
      $timeRemaining = $('.time-remaining');

  var modal = function() {
    $.modal.defaults = {
      overlay: "#000", // Overlay color
      opacity: 0.75, // Overlay opacity
      zIndex: 999, // Overlay z-index.
      escapeClose: true, // Allows the user to close the modal by pressing `ESC`
      clickClose: true, // Allows the user to close the modal by clicking the overlay
      closeText: '<i class="icon icon-close"></i>', // Text content for the close <a> tag.
      closeClass: 'modal-close', // Add additional class(es) to the close <a> tag.
      showClose: true, // Shows a (X) icon/link in the top-right corner
      modalClass: "modal", // CSS class added to the element being displayed in the modal.
      spinnerHtml: null, // HTML appended to the default spinner during AJAX requests.
      showSpinner: true, // Enable/disable the default spinner during AJAX requests.
      fadeDuration: null, // Number of milliseconds the fade transition takes (null means no transition)
      fadeDelay: 1.0 // Point during the overlay's fade-in that the modal begins to fade in (.5 = 50%, 1.5 = 150%, etc.)
    };
  };

  var graph = function() {
    $graph.highcharts({
      title: null,
      chart: {
        type: 'spline'
      },
      xAxis: {
        type: 'datetime',
        tickPixelInterval: 150
      },
      yAxis: [{ // Primary yAxis
        title: {
          text: 'Download',
          style: {
            color: '#aaa'
          }
        },
        labels: {
          format: '{value} Mbits',
          style: {
            color: '#aaa'
          }
        },
        floor: 0,
        maxPadding: 0.5
      }, { // Secondary yAxis
        title: {
          text: 'Upload',
          style: {
            color: '#aaa'
          }
        },
        labels: {
          format: '{value} Mbits',
          style: {
            color: '#aaa'
          }
        },
        floor: 0,
        opposite: true
      }],
      plotOptions: {
        spline: {
          lineWidth: 2,
          states: {
            hover: {
              lineWidth: 4
            }
          },
          marker: {
            enabled: false
          },
          pointInterval: 1000
        }
      },
      tooltip: {
        shared: true
      },
      legend: {
        layout: 'horizontal',
        backgroundColor: '#fff'
      },
      series: [{
        name: 'Download',
        type: 'spline',
        color: '#61a4f6',
        yAxis: 0,
        tooltip: {
          valueSuffix: ' Mbits'
        }
      },{
        name: 'Upload',
        type: 'spline',
        color: '#ff0000',
        yAxis: 1,
        tooltip: {
          valueSuffix: ' Mbits'
        }
      }]
    });
  };

  var helpers = function(){
    Handlebars.registerHelper('user_rate_unit', function() {
      return options.user_rate_unit;
    });

    Handlebars.registerHelper('total_rate_unit', function() {
      return options.total_rate_unit;
    });

    Handlebars.registerHelper('check_id', function(args) {
      return ( options.user_id == args ) ? 'user' : '';
    });

    Handlebars.registerHelper('check_priority', function(args) {
      if ( args ) {
        options.totalPriorities = options.totalPriorities + 1;
      }
      return ( args ) ? 'priority-true' : 'priority-false';
    });
  };

  var websocket = function() {
    options.ws = new WebSocket("ws://localhost:8082/ws");

    options.ws.onmessage = function (evt) {
      var jsonData = jQuery.parseJSON(evt.data);
      console.log(jsonData.message_type);
      fn[jsonData.message_type](jsonData);
    };
  };

  fn.setup_data_message = function(data) {
    options = $.extend( options, data );
    $('.user-ip').text( 'Meu ip: ' + options.ip_address );
    $('.max-total-download-rate').text( options.max_total_download_rate + ' ' + options.total_rate_unit );
    $('.max-total-upload-rate').text( options.max_total_upload_rate + ' ' + options.total_rate_unit );
    $since.text( 'Desde: ' + options.accumulated_since );
    $timeRemaining.text( '(' + options.defalult_priority_period + ')' );
    helpers();
  };

  fn.instant_data_message = function(data) {

    options.priority_users_count = data.priority_users_count;

    $activeUsersCount.text( data.active_users_count );

    $totalDislikes.text( data.accumulated_dislikes );

    $totalPriorities.text( options.priority_users_count + '/' + options.max_priority_users_count );

    if ( options.priority_users_count >= options.max_priority_users_count ){
      $btnPriority.addClass('btn-disabled');
    }

    $totalDownload.text( data.accumulated_download );

    $totalUpload.text( data.accumulated_upload + ' ' + options.total_rate_unit );

    //Ordena os arrays conforme o rank
    data.instant_download_users_data.sort(function(a,b) { return parseFloat(a.rank) - parseFloat(b.rank) } );
    data.instant_upload_users_data.sort(function(a,b) { return parseFloat(a.rank) - parseFloat(b.rank) } );

    options.user = $.grep(data.instant_download_users_data, function(e){ return e.user_id == options.user_id });
    if ( options.user[0].has_priority ) {
      $timeRemaining.text( '(' + options.user[0].priority_remaining_time + ')' );
    } else {
      $timeRemaining.text( '(' + options.defalult_priority_period + ')' );
      if ( options.priority_users_count < options.max_priority_users_count ){
        $btnPriority.removeClass('btn-disabled');
      }
    }

    var htmlDownload = templates.userTable(data.instant_download_users_data);
    $userTableDownload.html(htmlDownload);

    var htmlUpload = templates.userTable(data.instant_upload_users_data);
    $userTableUpload.html(htmlUpload);

    $totalInstantDownloadRate.text(data.total_instant_download_rate + ' ' + options.total_rate_unit);
    var downloadDegreesFill = (180 * data.total_instant_download_percentage/100) - 180;
    $totalInstantDownloadFill.css({
      '-webkit-transform' : 'rotate('+downloadDegreesFill+'deg)',
         '-moz-transform' : 'rotate('+downloadDegreesFill+'deg)',
          '-ms-transform' : 'rotate('+downloadDegreesFill+'deg)',
           '-o-transform' : 'rotate('+downloadDegreesFill+'deg)',
              'transform' : 'rotate('+downloadDegreesFill+'deg)'
    });
    var downloadDegreesPointer = (200 * data.total_instant_download_percentage/100) - 100;
    $totalInstantDownloadPointer.css({
      '-webkit-transform' : 'rotate('+downloadDegreesPointer+'deg)',
         '-moz-transform' : 'rotate('+downloadDegreesPointer+'deg)',
          '-ms-transform' : 'rotate('+downloadDegreesPointer+'deg)',
           '-o-transform' : 'rotate('+downloadDegreesPointer+'deg)',
              'transform' : 'rotate('+downloadDegreesPointer+'deg)'
    });

    $totalInstantUploadRate.text(data.total_instant_upload_rate + ' ' + options.total_rate_unit);
    var uploadDegreesFill = (180 * data.total_instant_upload_percentage/100) - 180;
    $totalInstantUploadFill.css({
      '-webkit-transform' : 'rotate('+uploadDegreesFill+'deg)',
         '-moz-transform' : 'rotate('+uploadDegreesFill+'deg)',
          '-ms-transform' : 'rotate('+uploadDegreesFill+'deg)',
           '-o-transform' : 'rotate('+uploadDegreesFill+'deg)',
              'transform' : 'rotate('+uploadDegreesFill+'deg)'
    });
    var uploadDegreesPointer = (200 * data.total_instant_upload_percentage/100) - 100;
    $totalInstantUploadPointer.css({
      '-webkit-transform' : 'rotate('+uploadDegreesPointer+'deg)',
         '-moz-transform' : 'rotate('+uploadDegreesPointer+'deg)',
          '-ms-transform' : 'rotate('+uploadDegreesPointer+'deg)',
           '-o-transform' : 'rotate('+uploadDegreesPointer+'deg)',
              'transform' : 'rotate('+uploadDegreesPointer+'deg)'
    });

  };

  fn.historical_rate_data_message = function(data) {
    var chart = $graph.highcharts();
    if (data.download_rate_values.length > 1 ) {
      var newData = [];
      $.each(data.download_rate_values, function(index, value){
        newData.push({
          x: new Date(value[0]).getTime(),
          y: value[1]
        });
      });
      newData.sort(function(a,b) { return parseFloat(a.x) - parseFloat(b.x) } );
      chart.series[0].setData(newData);
    } else {
      var newData = data.download_rate_values[0];
      newData[0] = new Date(newData[0]).getTime();
      chart.series[0].addPoint(newData, true, true);
    }
    if (data.upload_rate_values.length > 1 ) {
      var newData = [];
      $.each(data.download_rate_values, function(index, value){
        newData.push({
          x: new Date(value[0]).getTime(),
          y: value[1]
        });
      });
      newData.sort(function(a,b) { return parseFloat(a.x) - parseFloat(b.x) } );
      chart.series[1].setData(newData);
    } else {
      var newData = data.upload_rate_values[0];
      newData[0] = new Date(newData[0]).getTime();
      chart.series[1].addPoint(newData, true, true);
    }
  };

  fn.dislike_data_message = function(data) {
    var chart = $graph.highcharts();
    $.each(data.values, function(index, value){
      chart.xAxis[0].addPlotLine({
        value: new Date(value[0]).getTime(),
        color: '#ccc',
        width: 1,
        label: {
          text: 'Dislike (' + value[1] + ')'
        }
      });
    });
  };

  fn.console_message = function(data) {
    if ( options.console ){
      var html = '';
      $.each(data.message_content, function(index, value){
        html += '<p>'+value+'</p>';
      });
      $board.append(html);
    }
  };

  fn.user_action_response_message = function(data) {
    if ( data.response ){
      fn[data.action](data);
    }else {
      modalMessage('Ação não autorizada');
    }
  };

  fn.activate_console_action = function(data) {
    options.console = data.response;
  }

  fn.deactivate_console_action = function(data) {
    options.console = false;
    $board.empty();
  }

  fn.turn_off_action = function(data) {
    //modalMessage('Servidor será desligado');
  };

  fn.turn_on_action = function(data) {
    //modalMessage('Servidor foi religado');
  };

  fn.server_action_message = function(data) {
    if ( data.action === 'turn_off_action') {
      window.clearTimeout(options.modalTimer);
      modalService();
    } else if ( data.action === 'turn_on_action' ) {
      modalMessage('Servidor foi religado');
    }
  };

  fn.dislike_action = function(data) {
    if ( data.response ) {
      $btnDislike.removeClass('btn-disabled');
    }
  };

  fn.give_priority_action = function(data) {
    if ( data.response ) {
      modalMessage('Liberado');
    }else {
      modalMessage(data.response_reason);
    }
  };

  var modalMessage = function(message) {
    var obj = {};
    obj.message = message;
    var htmlUpload = templates.modalMessage(obj);
    $('body').append(htmlUpload);

    var $modalMessage = $('#modal-message');

    $modalMessage.on($.modal.BEFORE_OPEN, function(event, modal) {
      modal.blocker.hide();
      options.modalTimer = window.setTimeout(function(){
        $.modal.close();
      }, 2000);
    });

    $modalMessage.modal({
      showClose: false
    });

    $modalMessage.on($.modal.CLOSE, function(event, modal){
      $modalMessage.remove();
    });
  };

  var modalService = function() {
    var htmlUpload = templates.modalService();
    $('body').append(htmlUpload);

    var $modalService = $('#modal-service');

    $modalService.modal({
      showClose: false,
      escapeClose: false,
      clickClose: false
    });

    $modalService.find('.btn-yes').on('click', function(e){
      e.preventDefault();
      options.ws.send('{"message_type": "user_action_message","action": "turn_on_action"}');
      $.modal.close();
    });

    $modalService.on($.modal.CLOSE, function(event, modal){
      $modalService.remove();
    });
  };

  var modalPanic = function() {
    var htmlUpload = templates.modalPanic();
    $('body').append(htmlUpload);

    var $modalPanic = $('#modal-panic');
    $modalPanic.modal();

    $modalPanic.find('.btn-no').on('click', function(e){
      e.preventDefault();
      $.modal.close();
      $btnPanic.removeClass('btn-disabled');
    });

    $modalPanic.find('.btn-yes').on('click', function(e){
      e.preventDefault();
      options.ws.send('{"message_type": "user_action_message","action": "turn_off_action","user_id":"' + options.user_id + '"}');
      $.modal.close();
      $btnPanic.removeClass('btn-disabled');
    });

    $modalPanic.on($.modal.CLOSE, function(event, modal){
      $modalPanic.remove();
    });
  };

  var events = function() {
    $('#events').on($.modal.OPEN, function(event, modal) {
      options.ws.send('{"message_type": "user_action_message","action": "activate_console_action"}');
    }).on($.modal.CLOSE, function(event, modal) {
      options.ws.send('{"message_type": "user_action_message","action": "deactivate_console_action"}');
    });
  };

  var actions = function() {
    $btnDislike.on('click', function(e){
      e.preventDefault();
      if ( !$(this).hasClass('btn-disabled') ){
        options.ws.send('{"message_type": "user_action_message","action": "dislike_action"}');
        $(this).addClass('btn-disabled');
      }
    });

    $btnPanic.on('click', function(e){
      e.preventDefault();
      $(this).addClass('btn-disabled');
      modalPanic();
    });

    $btnPriority.on('click', function(e){
      e.preventDefault();
      if ( options.priority_users_count < options.max_priority_users_count ){
        options.ws.send('{"message_type": "user_action_message","action": "give_priority_action","target_user_id":"' + options.user_id + '"}');
        $(this).addClass('btn-disabled');
      }
    });
  }

  var init = function() {
    graph();
    websocket();
    events();
    actions();
  }();

  return exports;

})(jQuery);



