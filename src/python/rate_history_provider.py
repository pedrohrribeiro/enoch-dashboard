'''
Created on Dec 29, 2014

@author: ribeiro
'''
from base_provider import BaseProvider
from constants import *
from util import FormatDateTime, FormatNumber
from random import random


class RateHistoryProvider(BaseProvider):
    '''
    classdocs
    '''

    def __init__(self):
        BaseProvider.__init__(self)
        
        self._next_sending_time = None
        
        
        
    def _DoGetMessages(self, time, message_number):
        from mock_websocket_server import MockWSHandler
        
        
        if message_number == 1:
            self._next_sending_time = time + MockWSHandler.PERIOD_BETWEEN_EACH_RATE_HISTORY_DATA
            return self._ObtainFirstHistoryRateDataMessage(time)
        
        elif time >= self._next_sending_time:
            
            time_str = FormatDateTime(time)
            
            message = {
                MESSAGE_TYPE : RATE_HISTORY_DATA_MESSAGE,
                DOWNLOAD_RATE_VALUES : [[time_str, FormatNumber(random() * MockWSHandler.TOTAL_DOWNLOAD_MBIT)],],
                UPLOAD_RATE_VALUES : [[time_str, FormatNumber(random() * MockWSHandler.TOTAL_UPLOAD_MBIT)],],
            } 
            
            self._next_sending_time = time + MockWSHandler.PERIOD_BETWEEN_EACH_RATE_HISTORY_DATA
            
            return message
    

    def _ObtainFirstHistoryRateDataMessage(self, time):
        from mock_websocket_server import MockWSHandler
        
        time_now = time
        
        
        download_rate = MockWSHandler.TOTAL_DOWNLOAD_MBIT
        upload_rate = MockWSHandler.TOTAL_UPLOAD_MBIT / 2.0
        
        download_rate_values = []
        upload_rate_values = []
        
        initial_time = time_now - MockWSHandler.RATE_HISTORY_TIME_WINDOW
         
        while time_now > initial_time: 
            time_now -= MockWSHandler.PERIOD_BETWEEN_EACH_RATE_HISTORY_DATA
            download_rate = download_rate * .99
            download_rate_values.append([FormatDateTime(time_now), FormatNumber(download_rate)])
            upload_rate_values.append([FormatDateTime(time_now), FormatNumber(upload_rate)])
            
        message = {
            MESSAGE_TYPE : RATE_HISTORY_DATA_MESSAGE,
            DOWNLOAD_RATE_VALUES : download_rate_values,
            UPLOAD_RATE_VALUES : upload_rate_values,
        }
        
        return message
    
