'''
Created on Dec 27, 2014

@author: ribeiro
'''
from base_provider import BaseProvider
from constants import *
from util import FormatNumber, FormatDateTime, FormatTimeDelta
from copy import copy
from random import random
from dateutil import parser as date_parser


class InstantRateProvider(BaseProvider):
    
    USERS_COUNT = 100
    CHOOSEN_INDEXES = [5, 10, 20, 30, 40, 50] # The indexes of the users that will use more bandwidth in simulation 
    
    
    def __init__(self):
        BaseProvider.__init__(self)
        
        self._download_users_data = []
        self._upload_users_data = []
        
        self._change_direction = 1
        
        self._priority_users_count = 0
        
        self._InitializeUsersData()
        
        self._response = None
    
    
    def PutMessage(self, time, message):
        from mock_websocket_server import MockWSHandler
        
        if message[MESSAGE_TYPE] == USER_ACTION_MESSAGE and message[ACTION] == GIVE_PRIORITY_ACTION:
            if self._priority_users_count < MockWSHandler.MAX_PRIORITY_USERS_COUNT:
                
                self.GivePriorityToUser(time, message[TARGET_USER_ID])
                
                response = {
                    MESSAGE_TYPE : USER_ACTION_RESPONSE_MESSAGE,
                    ACTION : GIVE_PRIORITY_ACTION,
                    RESPONSE : True,
                }
            
            else:
                response = {
                    MESSAGE_TYPE : USER_ACTION_RESPONSE_MESSAGE,
                    ACTION : GIVE_PRIORITY_ACTION,
                    RESPONSE : False,
                    RESPONSE_REASON : 'Maximum priority users count reached.'                    
                }
            
            self._response = response
    
    
    def GivePriorityToUser(self, time_now, user_id, priority_period=None):
        if priority_period is None:
            from mock_websocket_server import MockWSHandler
            priority_period = MockWSHandler.DEFAULT_PRIORITY_PERIOD
            
        for users_data in (self._download_users_data, self._upload_users_data):
            for user_data in users_data:
                if user_data[USER_ID] == user_id:
                    break
            
            user_data[HAS_PRIORITY] = True
            user_data[PRIORITY_START_TIME] = FormatDateTime(time_now)
            user_data[PRIORITY_END_TIME] = FormatDateTime(time_now + priority_period)
            user_data[PRIORITY_REMAINING_TIME] = FormatTimeDelta(priority_period)
            user_data[PRIORITY_REMAINING_FRACTION] = 100.0
        
    
    def _UpdateUserPriorityData(self, time):
        for users_data in (self._download_users_data, self._upload_users_data):
            for user_data in users_data:
                if user_data[HAS_PRIORITY]:
                    start_time = date_parser.parse(user_data[PRIORITY_START_TIME])
                    end_time = date_parser.parse(user_data[PRIORITY_END_TIME])
                    remaining_time = end_time - time
                    if remaining_time.total_seconds() > 0.0:
                        user_data[PRIORITY_REMAINING_TIME] = FormatTimeDelta(remaining_time)
                        user_data[PRIORITY_REMAINING_FRACTION] = FormatNumber(100. * FormatNumber(remaining_time.total_seconds() / (end_time - start_time).total_seconds()))
                    else:
                        user_data[HAS_PRIORITY] = False
                        del  user_data[PRIORITY_START_TIME]
                        del user_data[PRIORITY_END_TIME]
                        del user_data[PRIORITY_REMAINING_TIME]
                        del user_data[PRIORITY_REMAINING_FRACTION]
                        
        self._priority_users_count = len([user_data for user_data in self._download_users_data if user_data[HAS_PRIORITY]])
                        
                        
    def _InitializeUsersData(self):
        from mock_websocket_server import MockWSHandler
        
        initial_share = .5 * (1. / 100.)
        
        for i in range(self.USERS_COUNT):
            user_id = 'User%d' %(i)
            
            user_data = {
                USER_ID : user_id,
                IP_ADDRESS : '192.168.1.%d' %i,
                INSTANT_RATE : FormatNumber(initial_share * MockWSHandler.TOTAL_DOWNLOAD_KBPS),  
                INSTANT_PERCENTAGE : FormatNumber(initial_share * 100.),
                RANK : i,  
                HAS_PRIORITY : False 
            }
            
            self._download_users_data.append(user_data)
            self._upload_users_data.append(copy(user_data))
            
    
    def _SumTotalRates(self, users_rate_data):
        total = 0
        for item in users_rate_data:
            total += item[INSTANT_RATE]
        
        return total
    
    
    def _UpdateUserRatesAndRank(self):
        
        from mock_websocket_server import MockWSHandler
        
        def UpdateRank(users_data_list):
            ranking_list = []
            i = 0
            for user_data in users_data_list:
                ranking_list.append((user_data[INSTANT_RATE], i))
                i += 1
                        
            ranking_list.sort(reverse=True)
            
            i = 0
            for item in ranking_list:
                users_data_list[item[1]][RANK] = i
                i += 1
                
        def UpdateRates(users_data_list):
            for i in self.CHOOSEN_INDEXES:
                user_data = users_data_list[i]
                change_factor = random() * .1 # Varies from .0 to 0.1
                user_data[INSTANT_RATE] = FormatNumber(user_data[INSTANT_RATE] + self._change_direction * user_data[INSTANT_RATE] * change_factor)
                user_data[INSTANT_PERCENTAGE] = FormatNumber(user_data[INSTANT_PERCENTAGE] + self._change_direction * user_data[INSTANT_PERCENTAGE] * change_factor)
             
        
        UpdateRates(self._download_users_data)
        UpdateRates(self._upload_users_data)
        
        if self._change_direction == 1 and self._SumTotalRates(self._download_users_data) > .95 * MockWSHandler.TOTAL_DOWNLOAD_KBPS:
            self._change_direction = -1
        elif self._change_direction == -1 and self._SumTotalRates(self._download_users_data) < .5 * MockWSHandler.TOTAL_DOWNLOAD_KBPS:
            self._change_direction = +1
        
        UpdateRank(self._download_users_data)
        UpdateRank(self._upload_users_data)
        
    
        
    def _DoGetMessages(self, time, message_number):
        from mock_websocket_server import MockWSHandler
        
        self._UpdateUserRatesAndRank()
        self._UpdateUserPriorityData(time)
        
        total_instant_dowload_rate_mbit = self._SumTotalRates(self._download_users_data) / 125.
        total_instant_dowload_percentage = 100. * total_instant_dowload_rate_mbit / MockWSHandler.TOTAL_DOWNLOAD_MBIT
        
        total_instant_upload_rate_mbit = self._SumTotalRates(self._upload_users_data) / 125.
        total_instant_upload_percentage = 100. * total_instant_upload_rate_mbit / MockWSHandler.TOTAL_UPLOAD_MBIT
            
        message = {
            MESSAGE_TYPE : INSTANT_DATA_MESSAGE,    
            TIME : FormatDateTime(time),
            ACTIVE_USERS_COUNT : time.minute,
            ACCUMULATED_DISLIKES : time.minute,
            ACCUMULATED_DOWNLOAD : time.minute,
            ACCUMULATED_UPLOAD : time.minute,
            PRIORITY_USERS_COUNT : self._priority_users_count,
            TOTAL_INSTANT_DOWNLOAD_RATE : FormatNumber(total_instant_dowload_rate_mbit),
            TOTAL_INSTANT_DOWNLOAD_PERCENTAGE : FormatNumber(total_instant_dowload_percentage),
            TOTAL_INSTANT_UPLOAD_RATE : FormatNumber(total_instant_upload_rate_mbit),
            TOTAL_INSTANT_UPLOAD_PERCENTAGE : FormatNumber(total_instant_upload_percentage),
        }

        message[INSTANT_DOWNLOAD_USERS_DATA] = self._SelectUsersForMessage(self._download_users_data)        
        message[INSTANT_UPLOAD_USERS_DATA] = self._SelectUsersForMessage(self._upload_users_data)
        
        if not self._response:
            return message
        else:
            response = self._response
            self._response = None
            return [response, message]
        
        
    
    def _SelectUsersForMessage(self, users_data_list):
        
        from mock_websocket_server import MockWSHandler
        
        users_data_for_message = []
        
        for user_data in users_data_list:
            if user_data[USER_ID] == MockWSHandler.TESTER_USER_ID or user_data[HAS_PRIORITY] or user_data[RANK] < 12:
                users_data_for_message.append(user_data)
        
        return users_data_for_message