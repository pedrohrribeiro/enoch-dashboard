'''
Created on Dec 29, 2014

@author: ribeiro
'''
from base_provider import BaseProvider
from constants import *
from util import FormatDateTime
from datetime import timedelta


class DislikeProvider(BaseProvider):
    '''
    classdocs
    '''

    def __init__(self):
        BaseProvider.__init__(self)
        
        self._message_to_send = None
        

    def _DoGetMessages(self, time, message_number):
        if message_number == 1:
            
            initial_time = time

            values = []
            
            for i in [10, 50, 68, 100, 120]:
                values.append(
                    [FormatDateTime(initial_time - i * timedelta(minutes=5)), 'User%d' %i])
            
            
            message = {
                MESSAGE_TYPE : DISLIKE_DATA_MESSAGE,
                VALUES : values,
            }
            
            return message
        
        if self._message_to_send is not None:
            
            message = self._message_to_send
            self._message_to_send = None
            return message
    
    
    def PutMessage(self, time, message):
        from mock_websocket_server import MockWSHandler
        
        if message[MESSAGE_TYPE] == USER_ACTION_MESSAGE and message[ACTION] == DISLIKE_ACTION:
            response = {
                MESSAGE_TYPE : USER_ACTION_RESPONSE_MESSAGE,
                ACTION : DISLIKE_ACTION,
                RESPONSE : True
            }
            
            dislike_message = {
                MESSAGE_TYPE : DISLIKE_DATA_MESSAGE,
                VALUES : [[FormatDateTime(time), MockWSHandler.TESTER_USER_ID], ]
            }
            self._message_to_send = [response, dislike_message]