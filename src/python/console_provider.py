from base_provider import BaseProvider
from constants import *


class ConsoleProvider(BaseProvider):
    
    def __init__(self):
        BaseProvider.__init__(self)
        self._active = False
        self._response = None
        
        
    def PutMessage(self, time, message):
        if message[MESSAGE_TYPE] == USER_ACTION_MESSAGE and message[ACTION] == ACTIVATE_CONSOLE_ACTION:
            self._response = [
            {
                MESSAGE_TYPE : USER_ACTION_RESPONSE_MESSAGE,
                ACTION : ACTIVATE_CONSOLE_ACTION,
                RESPONSE : True
            },
            {
                    MESSAGE_TYPE : CONSOLE_MESSAGE,
                    MESSAGE_CONTENT : ['Old Console message: Something hapenned! ', 'Old Console message: Something hapenned! ']
            }]                  
            self._active = True
            
        elif message[MESSAGE_TYPE] == USER_ACTION_MESSAGE and message[ACTION] == DEACTIVATE_CONSOLE_ACTION:
            self._response = {
                MESSAGE_TYPE : USER_ACTION_RESPONSE_MESSAGE,
                ACTION : DEACTIVATE_CONSOLE_ACTION,
                RESPONSE : True
            }
            self._active = False
        

    def _DoGetMessages(self, time, message_number):
        messages = []
        
        if self._response is not None:
            if isinstance(self._response, list):
                messages += self._response
            else:
                messages.append(self._response)
                
            self._response = None
            
        if self._active:
            console_message = {
                    MESSAGE_TYPE : CONSOLE_MESSAGE,
                    MESSAGE_CONTENT : ['Console message: Something hapenned! ',]
                }
            messages.append(console_message)
        
        
        if len(messages) > 0:
            return messages
        else:
            return None