'''
Created on Nov 26, 2014

@author: ribeiro
'''
import unittest
from tornado.testing import AsyncHTTPTestCase
from functools import partial
import websocket
from constants import *
from tornado.escape import json_decode
from mock_websocket_server import MockWSHandler


class BaseTestClient(websocket.WebSocket, unittest.TestCase):
    def __init__(self, *args, **kwargs):
        websocket.WebSocket.__init__(self, *args, **kwargs)
        self._messages_count = 0
        self._expected_messages = 300
    
    def _GetMessageToSend(self):
        return None
    
    
    def on_open(self):
        message_to_send = self._GetMessageToSend()
        
        if message_to_send is not None:
            self.write_message(message_to_send)


    def on_close(self, code, reason=None):
        pass
    
    
    def Terminate(self):
        pass


    def on_message(self, message):
        message = json_decode(message)
        self.assertIsInstance(message, dict)
        self._TestReceivedMessage(message)
        if self._messages_count >= self._expected_messages:
            self.Terminate()
            self.close()
            self.test_instance.stop()
            
            


class Test(AsyncHTTPTestCase):
    def get_app(self):
        from mock_websocket_server import application
        MockWSHandler.TICK_PERIOD_MS = 1
        MockWSHandler.CHECK_SCHEADULED_EVENTS = False
        MockWSHandler.MAX_PRIORITY_USERS_COUNT = 1
        return application
    
    
    def PerformTest(self, client_class):
        client_class.test_instance = self            
        self.io_loop.add_callback(
                partial(client_class, self.get_url('/ws'), self.io_loop))
        self.wait(timeout=2)
    

    def testGetHTMLFile(self):
        self.http_client.fetch(self.get_url('/web/dashboard.html'), self.stop)
        response = self.wait()
 
 
    def testSetupData(self):
        '''
        SETUP_DATA_MESSAGE
        
        This message holds data that is needed only once for setup. For now it only holds the used rate units.
        message = {
            MESSAGE_TYPE : SETUP_DATA_MESSAGE,
            USER_ID : '101',
            IP_ADDRESS : '192.168.1.5',
            TOTAL_RATE_UNIT : 'Mbit',
            USER_RATE_UNIT : 'Kbps'
            MAX_TOTAL_DOWNLOAD_RATE : '20',
            MAX_TOTAL_UPLOAD_RATE : '10',
            DEFAULT_PRIORITY_PERIOD : '00:60:00',
            MAX_PRIORITY_USERS_COUNT : 2,
        }
        '''
        
        class TestClient(BaseTestClient):
            
            def __init__(self, *args, **kwargs):
                BaseTestClient.__init__(self, *args, **kwargs)
                self._expected_messages = 1
            
            def _TestReceivedMessage(self, message):
                
                # The first received message should the the setup message
                self.assertEquals(message[MESSAGE_TYPE], SETUP_DATA_MESSAGE)
                self.assertTrue(USER_ID in message)
                self.assertTrue(IP_ADDRESS in message)
                self.assertTrue(TOTAL_RATE_UNIT in message)
                self.assertTrue(USER_RATE_UNIT in message)
                self.assertTrue(MAX_TOTAL_DOWNLOAD_RATE in message)
                self.assertTrue(MAX_TOTAL_UPLOAD_RATE in message)
                self.assertTrue(DEFAULT_PRIORITY_PERIOD in message)
                self.assertTrue(MAX_PRIORITY_USERS_COUNT in message)
                self.assertTrue(ACCUMULATED_SINCE in message)
                    
                self._messages_count += 1
                
        self.PerformTest(TestClient)    

 
    def testInstantData(self):
        '''
        INSTANT_DATA_MESSAGE
        
        This message holds all instant information and is sent periodically (eg. every second). 
         
        message = {
            MESSAGE_TYPE : INSTANT_DATA_MESSAGE,
            TIME : '2014-12-12T14:46:23',
            ACTIVE_USERS_COUNT : '23',
            TOTAL_INSTANT_DOWNLOAD_RATE : '17.22',
            TOTAL_INSTANT_DOWNLOAD_PERCENTAGE : '86.1',
            TOTAL_INSTANT_UPLOAD_RATE : '5.5',
            TOTAL_INSTANT_UPLOAD_PERCENTAGE : '55',
            
            INSTANT_DOWNLOAD_USERS_DATA : [
                {
                    USER_ID : '100',
                    IP_ADDRESS : '192.168.1.134',
                    INSTANT_RATE : '100.1',
                    INSTANT_PERCENTAGE : '10',
                    HAS_PRIORITY : 'False'
                },
                {
                    USER_ID : '101',
                    IP_ADDRESS : '192.168.1.135',
                    INSTANT_DOWNLOAD_RATE : '100.1',
                    INSTANT_DOWNLOAD_PERCENTAGE : '10',
                    INSTANT_UPLOAD_RATE : '192.7', 
                    INSTANT_UPLOAD_PERCENTAGE : '20.1',
                    HAS_PRIORITY : 'True',
                    PRIORITY_START_TIME : '2014-12-12T14:46:23':
                    PRIORITY_END_TIME : '2014-12-12T15:46:23'
                    PRIORITY_REMAINING_TIME : '0:30:40',
                    PRIORITY_REMAINING_FRACTION : '0.55'
                },
            ],
            
            INSTANT_UPLOAD_USERS_DATA : [
                {
                    USER_ID : '100',
                    IP_ADDRESS : '192.168.1.134',
                    INSTANT_RATE : '100.1',
                    INSTANT_PERCENTAGE : '10',
                    HAS_PRIORITY : 'False'
                },
                {
                    USER_ID : '101',
                    IP_ADDRESS : '192.168.1.135',
                    INSTANT_DOWNLOAD_RATE : '100.1',
                    INSTANT_DOWNLOAD_PERCENTAGE : '10',
                    INSTANT_UPLOAD_RATE : '192.7', 
                    INSTANT_UPLOAD_PERCENTAGE : '20.1',
                    HAS_PRIORITY : 'True',
                    PRIORITY_START_TIME : '2014-12-12T14:46:23':
                    PRIORITY_END_TIME : '2014-12-12T15:46:23'
                    PRIORITY_REMAINING_TIME : '0:30:40',
                    PRIORITY_REMAINING_FRACTION : '0.55'
                },
            ],
            
            }
        '''
        
        class TestClient(BaseTestClient):
            
            def _TestReceivedMessage(self, message):
                
                def CheckInstantUsersData(instant_users_data):
                    
                    for user_data in instant_users_data:
                        
                        self.assertTrue(USER_ID in user_data)
                        self.assertTrue(IP_ADDRESS in user_data)
                        self.assertTrue(INSTANT_RATE in user_data)
                        self.assertTrue(INSTANT_PERCENTAGE in user_data)
                        self.assertTrue(RANK in user_data)
                        self.assertTrue(HAS_PRIORITY in user_data)
                    
                        if user_data[HAS_PRIORITY]:
                            self.assertTrue(USER_ID in user_data)
                            self.assertTrue(PRIORITY_START_TIME in user_data)
                            self.assertTrue(PRIORITY_END_TIME in user_data)
                            self.assertTrue(PRIORITY_REMAINING_TIME in user_data)
                            self.assertTrue(PRIORITY_REMAINING_FRACTION in user_data)
                            
                
                if message[MESSAGE_TYPE] != INSTANT_DATA_MESSAGE:
                    return
                
                self.assertTrue(TIME in message)
                self.assertTrue(ACTIVE_USERS_COUNT in message)
                self.assertTrue(PRIORITY_USERS_COUNT in message)
                self.assertTrue(ACCUMULATED_DOWNLOAD in message)
                self.assertTrue(ACCUMULATED_UPLOAD in message)
                self.assertTrue(ACCUMULATED_DISLIKES in message)
                self.assertTrue(TOTAL_INSTANT_DOWNLOAD_RATE in message)
                self.assertTrue(TOTAL_INSTANT_DOWNLOAD_PERCENTAGE in message)
                self.assertTrue(TOTAL_INSTANT_UPLOAD_RATE in message)
                self.assertTrue(TOTAL_INSTANT_UPLOAD_PERCENTAGE in message)
    
                self.assertTrue(INSTANT_DOWNLOAD_USERS_DATA in message)
                self.assertTrue(INSTANT_UPLOAD_USERS_DATA in message)
                
                # Assume there is always at least one user in the test
                instant_download_users_data = message[INSTANT_DOWNLOAD_USERS_DATA]
                self.assertTrue(len(instant_download_users_data) > 0)
                CheckInstantUsersData(instant_download_users_data)
                
                instant_upload_users_data = message[INSTANT_UPLOAD_USERS_DATA]
                self.assertTrue(len(instant_upload_users_data) > 0)
                CheckInstantUsersData(instant_upload_users_data)
                    
                self._messages_count += 1
                
        self.PerformTest(TestClient)
    
    
    def testGiveMePriority(self):
        '''
        '''
        
        class TestClient(BaseTestClient):
            
            def __init__(self, *args, **kwargs):
                BaseTestClient.__init__(self, *args, **kwargs)
                self._messages_with_priority = MockWSHandler.DEFAULT_PRIORITY_PERIOD.total_seconds() / MockWSHandler.SIMULATION_TIME_PERIOD.total_seconds()
                self._expected_messages = self._messages_with_priority + 10
                self._received_response = False

                            
            def _TestReceivedMessage(self, message):

                def CheckUserPriorityData(user_data):
                    self.assertTrue(user_data[HAS_PRIORITY])
                    self.assertTrue(user_data[USER_ID] == MockWSHandler.TESTER_USER_ID)
                    self.assertTrue(PRIORITY_START_TIME in user_data)
                    self.assertTrue(PRIORITY_END_TIME in user_data)
                    self.assertTrue(PRIORITY_REMAINING_TIME in user_data)
                    self.assertTrue(PRIORITY_REMAINING_FRACTION in user_data)
                                    
                if message[MESSAGE_TYPE] == USER_ACTION_RESPONSE_MESSAGE and message[ACTION] == GIVE_PRIORITY_ACTION:
                    self.assertTrue(RESPONSE in message)
                    self.assertTrue(message[RESPONSE])
                    self._received_response = True
                    

                elif self._received_response and message[MESSAGE_TYPE] == INSTANT_DATA_MESSAGE:
                    instant_download_users_data = message[INSTANT_DOWNLOAD_USERS_DATA]
                    instant_upload_users_data = message[INSTANT_UPLOAD_USERS_DATA]

                    for users_data in (instant_download_users_data, instant_upload_users_data):
                        for user_data in users_data:
                            if user_data[USER_ID] == MockWSHandler.TESTER_USER_ID:
                                if self._messages_count < self._messages_with_priority:
                                    CheckUserPriorityData(user_data)
                                else:
                                    self.assertFalse(user_data[HAS_PRIORITY])
                                    self.assertFalse(PRIORITY_START_TIME in user_data)
                                    self.assertFalse(PRIORITY_END_TIME in user_data)
                                    self.assertFalse(PRIORITY_REMAINING_TIME in user_data)
                                    self.assertFalse(PRIORITY_REMAINING_FRACTION in user_data)
                            break
                    
                    self._messages_count += 1
                
            
            def _GetMessageToSend(self):
                message = {
                    MESSAGE_TYPE : USER_ACTION_MESSAGE,
                    USER_ID : MockWSHandler.TESTER_USER_ID,
                    ACTION : GIVE_PRIORITY_ACTION,
                    TARGET_USER_ID : MockWSHandler.TESTER_USER_ID
                }
                return message

                

        self.PerformTest(TestClient)
    
    
    
    def testGiveMePriorityMaximumUsersReached(self):
        '''
        '''
        
        class TestClient(BaseTestClient):
            
            def __init__(self, *args, **kwargs):
                BaseTestClient.__init__(self, *args, **kwargs)
                self._received_first_response = False

                            
            def _TestReceivedMessage(self, message):
                
                if message[MESSAGE_TYPE] == USER_ACTION_RESPONSE_MESSAGE and message[ACTION] == GIVE_PRIORITY_ACTION:
                    if not self._received_first_response:
                        self.assertTrue(message[RESPONSE])
                        self._received_first_response = True
                        
                        new_request_message = {
                            MESSAGE_TYPE : USER_ACTION_MESSAGE,
                            USER_ID : 'User6',
                            ACTION : GIVE_PRIORITY_ACTION,
                            TARGET_USER_ID : 'User6'
                        } 
                        self.write_message(new_request_message)
                    else:
                        self.assertFalse(message[RESPONSE])
                    
                self._messages_count += 1
                
            
            
            def _GetMessageToSend(self):
                message = {
                    MESSAGE_TYPE : USER_ACTION_MESSAGE,
                    USER_ID : MockWSHandler.TESTER_USER_ID,
                    ACTION : GIVE_PRIORITY_ACTION,
                    TARGET_USER_ID : MockWSHandler.TESTER_USER_ID
                }
                return message

                

        self.PerformTest(TestClient)


    def testRateHistoryData(self):
        '''
        RATE_HISTORY_DATA_MESSAGE
        
        This message holds data for the rate history graph. The first message sent should
        contain data to populate the whole graph (e.g. last 12 hours data). The following messages
        will be sent periodically (e.g. every 5 minutes) containing one value that is the averaged 
        upload/download rates of the last period.
        
        message = {
            MESSAGE_TYPE : RATE_HISTORY_DATA_MESSAGE,
            DOWNLOAD_RATE_VALUES : [ ['2014-12-12T14:46:23', '10.4'],  ['2014-12-12T14:51:23', '10.7'] ],
            UPLOAD_RATE_VALUES : [ ['2014-12-12T14:46:23', '10.4'],  ['2014-12-12T14:51:23', '10.7'] ],
        }
        '''
        
        class TestClient(BaseTestClient):
                        
            def _TestReceivedMessage(self, message):
                if message[MESSAGE_TYPE] != RATE_HISTORY_DATA_MESSAGE:
                    return
                
                self.assertTrue(DOWNLOAD_RATE_VALUES in message)
                self.assertTrue(UPLOAD_RATE_VALUES in message)
                    
                self._messages_count += 1
                

        self.PerformTest(TestClient)


    def testDislikeData(self):
        '''
        DISLIKE_DATA_MESSAGE
        
        Like the RATE_HISTORY_DATA_MESSAGE, the first message holds historical data of a given period 
        (e.g. last 12 hours) and the following ones will usually hold only one value.
        
        message = {
            MESSAGE_TYPE : DISLIKE_DATA_MESSAGE,
            VALUES = [['2014-12-12T14:46:23', 'User1'], ['2014-12-12T14:51:23', 'User2']],
        }
        '''
        
        class TestClient(BaseTestClient):
            
            def __init__(self, *args, **kwargs):
                BaseTestClient.__init__(self, *args, **kwargs)
                self._expected_messages = 3

                        
            def _TestReceivedMessage(self, message):
                
                if message[MESSAGE_TYPE] == DISLIKE_DATA_MESSAGE:
                    self.assertTrue(VALUES in message)
                    self._messages_count += 1
                
                elif message[MESSAGE_TYPE] == USER_ACTION_RESPONSE_MESSAGE and message[ACTION] == DISLIKE_ACTION:
                    self.assertTrue(RESPONSE in message)
                    self.assertTrue(message[RESPONSE])
                    self._messages_count += 1
                    
            
            def _GetMessageToSend(self):
                message = {
                    MESSAGE_TYPE : USER_ACTION_MESSAGE,
                    USER_ID : MockWSHandler.TESTER_USER_ID,
                    ACTION : DISLIKE_ACTION,
                }
                return message

                

        self.PerformTest(TestClient)
    
    
    
    def testTurnOff(self):
        '''
    
        '''
        
        class TestClient(BaseTestClient):
            
            def __init__(self, *args, **kwargs):
                BaseTestClient.__init__(self, *args, **kwargs)
                self._expected_messages = 2

                        
            def _TestReceivedMessage(self, message):
                
                if message[MESSAGE_TYPE] == USER_ACTION_RESPONSE_MESSAGE and message[ACTION] == TURN_OFF_ACTION:
                    self.assertTrue(RESPONSE in message)
                    self.assertTrue(message[RESPONSE])
                    self._messages_count += 1
                
                elif self._messages_count == 1 and message[MESSAGE_TYPE] == SERVER_ACTION_MESSAGE and message[ACTION] == TURN_OFF_ACTION:
                    self._messages_count += 1
                
            
            def _GetMessageToSend(self):
                message = {
                    MESSAGE_TYPE : USER_ACTION_MESSAGE,
                    USER_ID : MockWSHandler.TESTER_USER_ID,
                    ACTION : TURN_OFF_ACTION,
                }
                return message

                

        self.PerformTest(TestClient)
        
        
    def testTurnOn(self):
        '''
        '''
        
        class TestClient(BaseTestClient):
            
            def __init__(self, *args, **kwargs):
                BaseTestClient.__init__(self, *args, **kwargs)
                self._expected_messages = 2

                        
            def _TestReceivedMessage(self, message):
                
                if message[MESSAGE_TYPE] == USER_ACTION_RESPONSE_MESSAGE and message[ACTION] == TURN_ON_ACTION:
                    self.assertTrue(RESPONSE in message)
                    self.assertTrue(message[RESPONSE])
                    self._messages_count += 1
                
                elif self._messages_count == 1 and message[MESSAGE_TYPE] == SERVER_ACTION_MESSAGE and message[ACTION] == TURN_ON_ACTION:
                    self._messages_count += 1
                
            
            def _GetMessageToSend(self):
                message = {
                    MESSAGE_TYPE : USER_ACTION_MESSAGE,
                    USER_ID : MockWSHandler.TESTER_USER_ID,
                    ACTION : TURN_ON_ACTION,
                }
                return message

                

        self.PerformTest(TestClient)
        
    
    def testConsoleMessages(self):
        
        class TestClient(BaseTestClient):
            
            def __init__(self, *args, **kwargs):
                BaseTestClient.__init__(self, *args, **kwargs)
                self._console_active = False
                self._console_messages_count = 0

                            
            def _TestReceivedMessage(self, message):
                
                if message[MESSAGE_TYPE] == USER_ACTION_RESPONSE_MESSAGE:
                    if message[ACTION] == ACTIVATE_CONSOLE_ACTION:
                        self.assertTrue(message[RESPONSE])
                        self._console_active = True
                        
                    elif message[ACTION] == DEACTIVATE_CONSOLE_ACTION:
                        self.assertTrue(message[RESPONSE])
                        self._console_active = False
                    

                elif message[MESSAGE_TYPE] == CONSOLE_MESSAGE:
                    self.assertTrue(self._console_active)
                    self.assertTrue(MESSAGE_CONTENT in message)
                    self.assertIsInstance(message[MESSAGE_CONTENT], list)
                    self._console_messages_count += 1
                    
                    if self._console_messages_count == 10:
                        deactivate_message = {
                            MESSAGE_TYPE : USER_ACTION_MESSAGE,
                            ACTION : DEACTIVATE_CONSOLE_ACTION
                        } 
                        self.write_message(deactivate_message)

                self._messages_count += 1
    
            
            def Terminate(self):
                self.assertFalse(self._console_active)
                self.assertGreater(self._messages_count, self._console_messages_count)
            
            
            def _GetMessageToSend(self):
                return {
                    MESSAGE_TYPE : USER_ACTION_MESSAGE,
                    ACTION : ACTIVATE_CONSOLE_ACTION
                }

                

        self.PerformTest(TestClient)

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testPeriodicData']
    unittest.main()