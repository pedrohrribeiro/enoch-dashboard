'''
Created on Dec 29, 2014

@author: ribeiro
'''
from base_provider import BaseProvider
from constants import *


class TunOffProvider(BaseProvider):
    '''
    Mock provider for turn off and turn on messages.
    '''

    def __init__(self):
        BaseProvider.__init__(self)
        
        self._message_to_send = None
        self._on = True
    
    
    def GetOn(self):
        return self._on
        

    def _DoGetMessages(self, time, message_number):
        if self._message_to_send is not None:
            
            message = self._message_to_send
            self._message_to_send = None
            
            if message[0][ACTION] == TURN_OFF_ACTION:
                self._on = False
            return message
    
    
    def PutMessage(self, time, message):
        if message[MESSAGE_TYPE] == USER_ACTION_MESSAGE and message[ACTION] == TURN_OFF_ACTION:
            response = {
                MESSAGE_TYPE : USER_ACTION_RESPONSE_MESSAGE,
                ACTION : TURN_OFF_ACTION,
                RESPONSE : True
            }
            
            turn_off_message = {
                MESSAGE_TYPE : SERVER_ACTION_MESSAGE,
                ACTION : TURN_OFF_ACTION
            }
            
            self._message_to_send = [response, turn_off_message]
            
        
        elif message[MESSAGE_TYPE] == USER_ACTION_MESSAGE and message[ACTION] == TURN_ON_ACTION:
            response = {
                MESSAGE_TYPE : USER_ACTION_RESPONSE_MESSAGE,
                ACTION : TURN_ON_ACTION,
                RESPONSE : True
            }
            
            turn_on_message = {
                MESSAGE_TYPE : SERVER_ACTION_MESSAGE,
                ACTION : TURN_ON_ACTION
            }
            
            self._message_to_send = [response, turn_on_message]
            self._on = True

