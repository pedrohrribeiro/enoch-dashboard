def FormatNumber(number):
    return round(number, 2)


def FormatDateTime(date_time):
    return date_time.isoformat()


def FormatTimeDelta(time_delta):
    from datetime import date, datetime, time

    dt = datetime.combine(date.today(), time(0, 0)) + time_delta
    return dt.time().isoformat()