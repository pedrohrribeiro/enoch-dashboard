from constants import *
from util import FormatNumber
from base_provider import BaseProvider


class SetupProvider(BaseProvider):
    
    def _DoGetMessages(self, time, message_number):
        if message_number != 1:
            return None
        else:
            from mock_websocket_server import MockWSHandler
            message = {
                MESSAGE_TYPE : SETUP_DATA_MESSAGE,
                USER_ID : MockWSHandler.TESTER_USER_ID,
                IP_ADDRESS : '192.168.1.5',
                MAX_TOTAL_DOWNLOAD_RATE : FormatNumber(MockWSHandler.TOTAL_DOWNLOAD_MBIT),
                MAX_TOTAL_UPLOAD_RATE : FormatNumber(MockWSHandler.TOTAL_UPLOAD_MBIT),
                TOTAL_RATE_UNIT : 'Mbit',
                USER_RATE_UNIT : 'kbps',
                DEFAULT_PRIORITY_PERIOD  : '00:59:00',
                MAX_PRIORITY_USERS_COUNT : MockWSHandler.MAX_PRIORITY_USERS_COUNT,
                ACCUMULATED_SINCE : '08:00 a.m. today'
            }
            return message
             
        