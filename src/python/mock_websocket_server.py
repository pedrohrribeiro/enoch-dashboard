from tornado.websocket import WebSocketHandler
from tornado.ioloop import PeriodicCallback

import tornado.ioloop
import tornado.web

from constants import *
from datetime import datetime as dt, timedelta
from setup_provider import SetupProvider
from instant_rate_provider import InstantRateProvider
from rate_history_provider import RateHistoryProvider
from dislike_provider import DislikeProvider
from console_provider import ConsoleProvider
from turn_off_provider import TunOffProvider
 
 
 
class MockWSHandler(WebSocketHandler):
    
    SIMULATION_TIME_PERIOD = timedelta(seconds=60) # At each tick simulation advances 60s
    
    DEFAULT_PRIORITY_PERIOD = timedelta(minutes=40)
    
    MAX_PRIORITY_USERS_COUNT = 2
    
    TICK_PERIOD_MS = 1000 # At each 1s data is processed
    RATE_HISTORY_TIME_WINDOW = timedelta(hours=12)
    PERIOD_BETWEEN_EACH_RATE_HISTORY_DATA = timedelta(minutes=5)
    
    TESTER_USER_ID = 'User5'
    TOTAL_DOWNLOAD_MBIT = 10.0
    TOTAL_DOWNLOAD_KBPS = TOTAL_DOWNLOAD_MBIT * 125.
    
    TOTAL_UPLOAD_MBIT = 10.0
    TOTAL_UPLOAD_KBPS = TOTAL_UPLOAD_MBIT * 125.
    
    CHECK_SCHEADULED_EVENTS = True
    
    
    def __init__(self, *args):
        WebSocketHandler.__init__(self, *args)

        self._callback = PeriodicCallback(self._Tick, self.TICK_PERIOD_MS)

        self._time_now = dt(2014, 12, 17, 12) # starts at noon 12/17/14
        
        self._setup_provider = SetupProvider()
        self._rate_history_provider = RateHistoryProvider()
        self._dislike_provider = DislikeProvider()
        self._instant_rate_provider = InstantRateProvider()
        self._console_provider = ConsoleProvider()
        self._turn_off_provider = TunOffProvider()
        
        self._providers = [
            self._setup_provider, 
            self._rate_history_provider, 
            self._dislike_provider, 
            self._instant_rate_provider,
            self._console_provider,
            self._turn_off_provider
        ]
        
        
    def open(self):
        self._callback.start()
        
    
    def _Tick(self):
        if self._turn_off_provider.GetOn():
            messages = []
            for provider in self._providers:
                message = provider.GetMessages(self._time_now)
                if message is not None:
                    if isinstance(message, list):
                        for item in message:
                            messages.append(item)
                    else:                
                        messages.append(message)
            
            for message in messages:
                self.write_message(message) 
    
            
            if self.CHECK_SCHEADULED_EVENTS:
                self._CheckScheaduledEvents()
                
                
            self._time_now += self.SIMULATION_TIME_PERIOD
        
    
    def _CheckScheaduledEvents(self):
        if self._time_now == dt(2014, 12, 17, 12, 5):
            self._instant_rate_provider.GivePriorityToUser(self._time_now, 'User6')
        
        elif self._time_now == dt(2014, 12, 17, 12, 30):
            self._instant_rate_provider.GivePriorityToUser(self._time_now, 'User21')
        
        
      
    def on_message(self, message):
        message = tornado.escape.json_decode(message)
        
        action = message[ACTION]
        
        if action == DISLIKE_ACTION:
            self._dislike_provider.PutMessage(self._time_now, message)
        elif action == TURN_OFF_ACTION or action == TURN_ON_ACTION:
            self._turn_off_provider.PutMessage(self._time_now, message)
        elif action == GIVE_PRIORITY_ACTION:
            self._instant_rate_provider.PutMessage(self._time_now, message)
        elif action == ACTIVATE_CONSOLE_ACTION or action == DEACTIVATE_CONSOLE_ACTION:
            self._console_provider.PutMessage(self._time_now, message)
            
        
 
    def on_close(self):
        self._callback.stop()
    
    
 
application = tornado.web.Application([
    (r'/ws', MockWSHandler),
    (r'/web/(.*)', tornado.web.StaticFileHandler, {'path': '../web/'}),
])
 
 
if __name__ == "__main__":
    import tornado.httpserver
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8082)
    tornado.ioloop.IOLoop.instance().start()
