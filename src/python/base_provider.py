
class BaseProvider(object):
    
    def __init__(self):
        self._messages_received_counter = 0
    
    
    def PutMessage(self, time, message):
        return None


    def GetMessages(self, time):
        self._messages_received_counter += 1
        return self._DoGetMessages(time, self._messages_received_counter)
    
    
    def _DoGetMessages(self, time, message_number):
        return None
